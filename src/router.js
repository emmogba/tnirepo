import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import Register from '@/pages/Register'
import Login from '@/pages/Login'
import Reply from '@/pages/Reply'
import Verify from '@/pages/verify'
import Invite from '@/pages/Invite'
import DistInvite from '@/pages/DistInvite'
import Download from '@/pages/Download'
import Downloader from '@/pages/downloader'
// import Dashboardadmin from '@/dashboard1/pages/Dashboardadmin'
// folders route for Super_Admin and components starts here
import SuperAdmin from '@/SuperAdmin/Pages/SuperAdminDashboard'
import SuperTrans from '@/SuperAdmin/Components/SuperTranslation'
import SuperLang from '@/SuperAdmin/Components/SuperLanguage'
import SuperTeam from '@/SuperAdmin/Components/SuperTeam'
// folders route for Super_Admin and components ends here
// import Dashboard1 from '@/dashboard1/pages/Dashboard1'
// folders route for Teamlead and components starts here
import TeamLead from '@/TeamLead/Pages/TeamDashboard'
import TeamLang from '@/TeamLead/Components/TeamLanguage'
import TeamDown from '@/TeamLead/Components/TeamDownload'
import TeamDist from '@/TeamLead/Components/TeamDistributor'
import Team from '@/TeamLead/Components/Teams'
// folders route for Teamlead and components ends here
// import Dashboarddist from '@/dashboard1/pages/dashboarddist'
// folders route for Distributors and components starts here
import Distributor from '@/Distributor/Pages/DistributorDashboard'
import DistLang from '@/Distributor/Components/DistributorLanguage'
import DistDown from '@/Distributor/Components/DistributorDownload'
// folders route for Distributors and components ends here
// import Translation from '@/dashboard1/pages/Translation'
// import Teams from '@/dashboard1/pages/Teams'
// import Language from '@/dashboard1/pages/Languages'
import login from '@/store/modules/login'
// import store from '@/store/store'

Vue.use(Router)

 const router = new Router({
  routes: [
    {
      path: '',
      component: Home,
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/reply',
      name: 'reply',
      component: Reply,
    },
    {
      path: '/verify',
      name: 'verify',
      component: Verify,
      props: (route) => ({ query: route.query.q }),
    },
    {
      path: '/team/invite',
      name: 'invite',
      component: Invite,
      props: (route) => ({ query: route.query.q }),
    },
    {
      path: '/dist/invite',
      name: 'invite',
      component: DistInvite,
      props: (route) => ({ query: route.query.q }),
    },
    {
      path: '/dist/download/:link',
      name: 'download',
      component: Download,
      props: (route) => ({ query: route.query.q }),
    },
    {
      path: '/downloader',
      name: 'downloader',
      component: Downloader,
    },
    // {
    //   path: '/translation',
    //   name: 'translation',
    //   component: Translation,
    // },
    // {
    //   path: '/language',
    //   name: 'langauge',
    //   component: Language,
    // },
    // {
    //   path: '/team',
    //   name: 'team',
    //   component: Teams,
    // },
    // {
    //   path: '/dashboardadmin',
    //   name: 'dashboardadmin',
    //   component: Dashboardadmin,
    //   meta: {
    //     requiresAuth: true,
    //   },
    // },
    // folders route for Super_Admin and components
    {
      path: '/admindashboard',
      name: 'SuperAdmin',
      component: SuperAdmin,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/admintrans',
      name: 'supertrans',
      component: SuperTrans,
    },
    {
      path: '/adminlang',
      name: 'adminlang',
      component: SuperLang,
    },
    {
      path: '/adminteam',
      name: 'adminteam',
      component: SuperTeam,
    },
    // folders route for Super_Admin and components
    // {
    //   path: '/dashboard1',
    //   name: 'dashboard1',
    //   component: Dashboard1,
    //   // meta: {
    //   //   requiresAuth: true,
    //   // },
    // },
    // folders route for TeamLead and components starts here
    {
      path: '/teamlead',
      name: 'teamlead',
      component: TeamLead,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/teamlang',
      name: 'teamlang',
      component: TeamLang,
    },
    {
      path: '/teamdown',
      name: 'teamdown',
      component: TeamDown,
    },
    {
      path: '/teamdist',
      name: 'teamdist',
      component: TeamDist,
    },
    {
      path: '/team',
      name: 'team',
      component: Team,
    },
    // folders route for TeamLead and components ends here
    // {
    //   path: '/dashboarddist',
    //   name: 'dashboarddist',
    //   component: Dashboarddist,
    //   meta: {
    //     requiresAuth: true,
    //   },
    //   beforeEnter: (to, from, next) => {
    //
    //   },
    // },
    // folders route for Distributor and components starts here
    {
      path: '/distributor',
      name: 'distributor',
      component: Distributor,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/distdown',
      name: 'distdown',
      component: DistDown,
    },
    {
      path: '/distlang',
      name: 'distlang',
      component: DistLang,
    },
    // folders route for Distributor and components starts here
  ],
  mode: 'history',
  base: process.env.BASE_URL,
})
  router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth) {
      if (!login.state.token) {
        next('/login')
      } else {
        next()
      }
    } else {
      next()
    }
  })
export default router
