import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
// import router from '@/router'
// import axios from 'axios'

const vuexPersist = new VuexPersist({
  key: 'my-app',
  storage: window.localStorage,
  reducer: (state) => ({}),
  filter: (mutation) => mutation.type === '',
})
Vue.use(Vuex)
const login = {
    state: {
      username: '',
      email: '',
      token: null,
      userRole: [],
      distLink: '',
      transUrl: '',
      teamLink: '',
    },

    getters: {
      getToken: state => {
        return state.token
      },
      getUserRole: state => {
        return state.userRole
      },
      getEmail: state => {
        return state.email
      },
      getUsername: state => {
        return state.username
      },
      getDistLink: state => {
        return state.distLink
      },
      getTeamLink: state => {
        return state.teamLink
      },
    },

    mutations: {
      setToken: (state, token) => {
        state.token = token
      },
      setUserRole: (state, userRole) => {
        state.userRole = userRole
      },
      setEmail: (state, email) => {
        state.email = email
      },
      setUsername: (state, username) => {
        state.username = username
      },
      setDistLink: (state, distLink) => {
        state.distLink = distLink
      },
      setTeamLink: (state, teamLink) => {
        state.teamLink = teamLink
      },
    },

    actions: {
        login ({ commit }, formData) {
          // axios.post(this.baseUrl + '/dept/admin/tni/auth/authenticate')
          //   .then(res => {
          //     this.userRole = res.data.auth.userClaims.userRole
          //     if (this.userRole === 'DISTRIBUTOR') {
          //       router.push('/dashboarddist')
          //     }
          //     if (this.userRole === 'TEAMLEAD') {
          //       router.push('/dashboard1')
          //     }
          //     // commit('setEmail', res.data.auth.email)
          //     // commit('setToken', res.data.auth.token)
          //     // commit('setUserRole', res.data.auth.userClaims.userRole)
          //     // console.log(res.data)
          //   })
          //   .catch(err => alert('invalid Username or password')(err))
        },
    },
    plugins: [vuexPersist.plugin],
  }
  export default login
