import Vue from 'vue'
import Vuex from 'vuex'
import login from '@/store/modules/login'
import register from '@/store/modules/register'
import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
  storage: window.localStorage,
})

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    register: register,
    login: login,
    plugins: [vuexPersist.plugin],
  },
})
