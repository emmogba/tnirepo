// import axios from 'axios'

class UploadFilesService {
  upload (file, onUploadProgress) {
    const formData = new FormData()
    formData.append('file', file)

    return this.axios.post(this.baseUrl + '/api/translation/upload_ror', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      onUploadProgress,
    })
  }

  getFiles () {
    return this.axios.get(this.baseUrl + '/api/translation/')
  }
}

export default new UploadFilesService()
