import axios from 'axios'

export default axios.create({
  baseURL: 'http://localhost:8080', //  https://api.downloads.tniglobal.org
  headers: {
    'Content-type': 'application/json',
  },
})
