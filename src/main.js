// =========================================================
// * Vuetify Material Dashboard - v2.1.0
// =========================================================
//
// * Product Page: https://www.creative-tim.com/product/vuetify-material-dashboard
// * Copyright 2019 Creative Tim (https://www.creative-tim.com)
//
// * Coded by Creative Tim
//
// =========================================================
//
// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import './plugins/base'
import './plugins/chartist'
import './plugins/vee-validate'
import vuetify from './plugins/vuetify'
import i18n from './i18n'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuelidate from 'vuelidate'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHome } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import colors from 'vuetify/lib/util/colors'
import Vuetify from 'vuetify/lib'
import VueWow from 'vue-wow'
import vSelect from 'vue-select'
import Select2 from 'v-select2-component'
import 'vue-select/dist/vue-select.css'
import 'vue-select/src/scss/vue-select.scss'
import VueAnimateOnScroll from 'vue-animate-onscroll'
import VueCarousel from 'vue-carousel'
import VueGoodTablePlugin from 'vue-good-table'
import VueSocialSharing from 'vue-social-sharing'
import VueLocalStorage from 'vue-localstorage'
// import the styles
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
import 'vue-good-table/dist/vue-good-table.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import '@fortawesome/fontawesome-free/css/all.min.css'
import responsive from 'vue-responsive'
import * as mdbvue from 'mdbvue'
import Vuex from 'vuex'
for (const component in mdbvue) {
Vue.component(component, mdbvue[component])
}

Vue.use(Vuex)
Vue.use(Vuetify)
Vue.use(responsive)
Vue.use(VueWow)
Vue.use(VueAnimateOnScroll)
Vue.use(VueCarousel)
Vue.use(VueGoodTablePlugin)
Vue.component('v-select', vSelect)
Vue.component('Select2', Select2)
Vue.use(VueFormWizard)
Vue.use(VueSocialSharing)
Vue.use(VueLocalStorage)

export default new Vuetify({
  breakpoint: {
    thresholds: {
      xs: 340,
      sm: 540,
      md: 800,
      lg: 1280,
    },
    scrollBarWidth: 24,
  },
  theme: {
    themes: {
      light: {
        primary: colors.red.darken1, // #E53935
        secondary: colors.red.lighten4, // #FFCDD2
        accent: colors.indigo.base, // #3F51B5
      },
    },
  },
})

library.add(faHome)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Vuelidate)
Vue.use(VueAxios, axios)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.config.productionTip = false
Vue.mixin({
  data: function () {
    return {
      baseUrl: 'https://api.downloads.tniglobal.org', // https://api.downloads.tniglobal.org http://207.244.109.166:3001
    }
  },
})
new Vue({
  router,
  store,
  axios,
  vuetify,
  i18n,
  render: h => h(App),
}).$mount('#app')
