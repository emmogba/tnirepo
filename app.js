var express = require('express')
var logger = require('express-log-url')
var app = express()
var history = require('connect-history-api-fallback')
const http = require('http')
const fs = require('fs')
const httpPort = 8081
// var request = require('request');
var bodyParser = require('body-parser')
const config = require('./db/config.json')
var path = require('path')
const errorHandler = require('./_helper/error-handler')
// const middleware = require('./_helper/auth-middleware')
var router = express.Router()
// var path = __dirname + '/public/'
const cookieParser = require('cookie-parser')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
var dbConn = require('./db/mongoose')
var db = dbConn.db
var hostname = process.HOST || '0.0.0.0'
var port = process.PORT || 5000
const serveStatic = require('serve-static')

app.use(cookieParser())
app.use(session({
    secret: config.secret,
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: db }),
}))

app.use(errorHandler)
app.use('/api', router)
app.use(express.static('src'))
// app.use(require('express-log-url'))
app.use(history())

app.use("/",router)
// here we are configuring dist to serve app files
app.use('/', serveStatic(path.join(__dirname, '/dist')))

// this * route is to serve project on different page routes except root `/`
app.get('/', (req, res) => {
  res.sendFile('src/index.html')
})
 // app.get(/.*/, function (req, res) { res.sendFile(path.join(__dirname, '/dist/index.html')) })

app.use(bodyParser.urlencoded({ extended: true, limit: '100mb' }))
app.use(bodyParser.json({ limit: '50mb' }))
app.use(function (req, res, next) {
res.header('Access-Control-Allow-Methods', 'POST,GET,PUT,DELETE,OPTIONS')
res.header('Access-Control-Allow-Origin', '*')
res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
next()
 })
// All artisan routers
// logger.log({method: 'GET', code: 200, path: '/', responseTime: 10})
// var indexRouter = require('./routes');
  app.use('/dept/admin/tni/auth', require('./route/Authentication'))
  app.use('/dept/admin/tni/distributor', require('./route/Distributor'))
  app.use('/dept/admin/tni/user', require('./route/User'))
  app.use('/dept/admin/tni/distributions', require('./route/Distribution'))
  app.use('/dept/admin/tni/team', require('./route/Team'))
  app.use('/dept/admin/tni/language', require('./route/Language'))
  app.use('/dept/admin/tni/download', require('./route/Download'))
  app.use('/dept/admin/tni/translation', require('./route/Translation'))
  app.use('/firebase', require('./route/Config'))
 // mapping for frontend view

app.listen(port, hostname, () => {
    console.log(`Server running at port ${port}`)
    })

console.log(`nodejs server running on ${port}`)

module.exports = app
